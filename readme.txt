This is the template for an Android application.

It is based off of the android maven archetype provided here: https://github.com/akquinet/android-archetypes

Mvnvm is set to maven 3.0.5.
If you don't know what Mvnvm is, I strongly encourage you to check it out at http://mvnvm.org/.
It will change your maven life! :)

Please check the pomfile for dependencies.
